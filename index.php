<?php

use autoload\Autoload;
use core\Model;
use core\Router;
use helpers\ConfigHelper;

require_once 'autoload/Autoload.php';
(new Autoload())->register();

session_start();

header('Content-Type: text/html; charset=UTF-8');

define('DS', DIRECTORY_SEPARATOR);

define('ROOT', realpath(dirname(__FILE__)));

ConfigHelper::set();

if (ConfigHelper::get('core', 'mode') == 1) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

Router::getInstance()->goRouting();

//TODO: Переделать в личке все проверки на пост запрос, вместо редиректа рисовать 404
//TODO: Проверять при бронировании, заполнил ли юзер свою анкету
//TODO: Переделать адресную строку в отделе информации в футере. http://sandbox/info/?alias=platnie-uslugi-i-oplata/. Зайти через sandbox/info/
//TODO: Переписать .htaccess
//TODO: Перенести Jivosite, пиксели вк и facebook, ссылки на мобильное приложение