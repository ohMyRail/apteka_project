<?php

namespace lib;

use core\Config;

class SMSHandler {

    private $user = null;
    private $pass = null;

    //Без +7
    private $number = null;

    public function __construct($number = null)
    {

        $this->number = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        $this->user = Config::get('smsaero', 'login');
        $this->pass = md5(Config::get('smsaero', 'pass'));

        if (empty($this->user) || $this->user == '')
            exit('ERROR::LIB::SMS::username is empty');
        if (empty($this->pass) || $this->pass == '')
            exit('ERROR::LIB::SMS::userpass is empty');
    }

    public function prepare()
    {
        return '7' . $this->number;
    }

    public function send($text)
    {
        $this->setReceiver($this->number);

        if (is_null($this->number))
            return false;

        $msg = urlencode(filter_var($text, FILTER_SANITIZE_STRING));
        $url = "https://gate.smsaero.ru/send/?user={$this->user}&password={$this->pass}&to={$this->prepare()}&from=NEWS&text={$msg}&type=4&answer=json";

        if ($curl = curl_init()) {

            curl_setopt($curl, CURLOPT_URL, $url);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            if (Config::get('smsaero', 'http_method') == 'POST')
                curl_setopt($curl, CURLOPT_POST, true);
            else
                curl_setopt($curl, CURLOPT_HTTPGET, true);

            $out = curl_exec($curl);
            curl_close($curl);

            $responce = json_decode($out);

            if ($responce->result == 'accepted') {
                return true;
            }
            return false;
        }

        exit("ERROR::LIB::SMS::curl is not existed");
    }

    public function setReceiver($number)
    {
        $phone = filter_var($number, FILTER_SANITIZE_NUMBER_INT);
        $phone = str_replace(array('+7', '+8', '+9', '+6', '(', ')', '_', '-', ' '), '', $phone);

        $this->number = $phone;
    }

}