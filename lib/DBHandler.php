<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 03.08.2017
 * Time: 15:00
 */

namespace lib;

use helpers\ConfigHelper;
use helpers\LogHelper;
use PDO;
use PDOException;
use QueryBuilder;

class DBHandler
{
    private $db;
    private $tablePref = null;
    private static $instance = null;


    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->tablePref = ConfigHelper::get('db', 'pref');

        $this->db_connect(
            ConfigHelper::get('db', 'host'),
            ConfigHelper::get('db', 'port'),
            ConfigHelper::get('db', 'name'),
            ConfigHelper::get('db', 'user'),
            ConfigHelper::get('db', 'pass')
        );

        $this->lowQuery("SET time_zone = :timezone", array('timezone' => ConfigHelper::get('app', 'timezone')));
    }

    private function db_connect($host, $port, $dbname, $user, $pass)
    {
        try {
            $this->db = new PDO("mysql:host=$host;port=$port;dbname=$dbname;", $user, $pass);

            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->db->exec('SET NAMES utf8');
        } catch (PDOException $e) {
            exit("Нет соединения с базой данных");
        }
        return false;
    }

    public function getLastInsertedID()
    {
        return $this->db->lastInsertId();
    }

    /**
     * Функция SELECT конструирует  строку SQL запроса
     * @access public
     * @param string $table
     * @param array $needle
     * @param array $where
     * @param array $orderby
     * @param array $limit
     * @return resource id
     * @internal param string $table название таблицы
     * @internal param array $needle or string. Неасоциативный массив array('имя колонки','Имя колонки 2').
     *                               Если в параметр передается null  бужет выбран все колонки (*)
     * @internal param string $attr По умолчанию DESC, можно передавать ASC
     * @see columns()
     */
    public function select($table, $needle = array('*'), $where = array(), $orderby = array(), $limit = array()) {
        $values = null;
        $data = null;

        if (is_null($table) || !is_string($table)) {
            exit("Некорректное название  имени таблицы");
        }
        if (!is_array($needle) && !is_null($needle)) {
            exit("Некорректно передано значение столбцов");
        }
        if (!is_array($where) && !is_null($where)) {
            exit("Некорректно передано значения условия WHERE");
        }
        if (!is_array($orderby) && !is_null($orderby)) {
            exit("Некорректно передано значения сортировки");
        }
        if (!is_array($limit) && !is_null($limit)) {
            exit("Некорректно передано значение limit");
        }

        foreach ($where as $key => $value) {

            //Делаем выборку каждого элемента массива и сохранаем их всех ввиде одной строки "{$key}={$value} AND ..."
            if ($value == 'NOT NULL' || $value == 'NULL') {
                $data .= "`{$key}` IS $value AND ";
            } else {
                $data .= "`{$key}` = :{$key} AND ";
                $values[$key] = $value;
            }
        }

        $where = trim(trim($data), 'AND');

        $columns = (!is_null($needle)) ? $this->columns($needle) : '*';

        if (!empty($orderby)) {
            $values['ordercol'] = (isset($orderby[0])) ? $orderby[0] : 'id';

            $values['orderway'] = (isset($orderby[1])) ? $orderby[1] : 'DESC';
        }

        $limit_code = null;

        if (!empty($limit)) {

            if (isset($limit[0])) {
                $limit_code = "LIMIT " . (filter_var($limit[0], FILTER_SANITIZE_NUMBER_INT));
            }

            if (isset($limit[1])) {
                $limit_code .= ", " . (filter_var($limit[1], FILTER_SANITIZE_NUMBER_INT));
            }
        }
        $table = $this->tablePref . $table;

        $where = ( empty($where) ) ? null : "WHERE {$where}";

        $orderby = ( empty($orderby) ) ? null : "ORDER BY :ordercol :orderway";


        $query = "SELECT {$columns} FROM {$table} {$where} {$orderby} {$limit_code}";

        $stmt = $this->db->prepare($query);

        $stmt->execute($values);

        return $stmt;
    }

    public function count($table, $where, $as)
    {
        if (is_null($table) or !is_string($table)) {
            exit("Некорректное название  имени таблицы");
        }
        if (!is_array($where) and !is_null($where)) {
            exit("Некорректно передано значения условия WHERE");
        }
        if (is_null($as) or !is_string($as)) {
            exit("Некорректно передано значение AS");
        }

        $data = null;
        $values = null;

        foreach ($where as $key => $value) {
            //Делаем выборку каждого элемента массива и сохранаем их всех ввиде одной строки "{$key}={$value} AND ..."
            if ($value == 'NOT NULL' || $value == 'NULL') {
                $data .= "`{$key}` IS $value AND ";
            } else {
                $data .= "`{$key}` = :{$key} AND ";
                $values[$key] = $value;
            }
        }

        $where = trim(trim($data), 'AND');
        $where = (empty($where)) ? null : "WHERE {$where}";

        $as = "AS {$as}";

        $query = "SELECT COUNT(*) {$as} FROM {$table} {$where}";

        $stmt = $this->db->prepare($query);

        $stmt->execute($values);
        return $stmt;
    }

    /**
     * Функция lowQuery конструирует  сложную строку SQL запроса состоящей из нескольких подзапросов
     * @access public
     * @param $sql
     * @param null $data
     * @return resource id
     * @internal param string $table название таблицы
     * @internal param array $needle or string. Неасоциативный массив array('имя колонки','Имя колонки 2').
     *                               Если в параметр передается null  бужет выбран все колонки (*)
     * @internal param string $attr По умолчанию DESC можно передавать ASC
     * @see columns()
     */
    public function lowQuery($sql, $data = null) {

        if (!is_array($data) && !is_null($data)) {
            exit("Некорректные данные. Данные следует передавать в массиве. Error in " . __LINE__);
        }

        $stmt = $this->db->prepare($sql);

        if (is_array($data) && !is_null($data)) {
            $stmt->execute($data);
        } else {
            $stmt->execute();
        }

        return $stmt;
    }

    /**
     * Функция UPDATE конструирует  строку SQL запроса и передает готовую строку функции _query(SQL запрос)
     * @access public
     * @param string $table название таблицы
     * @param array $data асоциативный массив array('column_name'=>'value')
     * @param array $where Параметры для условия WHERE
     * @param null $limit
     * @return bool
     * @see columns()
     */
    public function update($table, $data, $where = null, $limit = null) {


        if (is_int($table) || empty($table))
            exit("Некорректное название  имени таблицы Error in " . __LINE__);

        if (!is_array($data) || empty($data))
            exit("Некорректные данные. Данные следует передавать в массиве. Error in " . __LINE__);

        if (!is_array($where))
            exit("Некорректные данные. Данные следует передавать в массиве. Error in " . __LINE__);


        $values = '';
        $setData = '';
        $whereData = '';
        $i = 1;

        foreach ($data as $column => $value) {
            $setData .= "`{$column}`=:{$column}, ";
            $values[$column] = $value;
        }
        foreach ($where as $key => $value) {

            $val = ( array_key_exists($key, $values) ) ? $key . $i : $key;

            $whereData .= "`{$key}` = :{$val} AND ";

            $values[$val] = $value;

            $i++;
        }

        $whereData = trim(trim($whereData), 'AND');

        $setData = trim(trim($setData), ',');

        $table = $this->tablePref . $table;

        $whereData = (is_null($where)) ? null : "WHERE {$whereData}";

        $limit = ( empty($limit) ) ? null : "LIMIT = {$limit}";

        $query = "UPDATE `{$table}` SET {$setData} {$whereData} {$limit}";

        $stmt = $this->db->prepare($query);

        $stmt->execute($values);

        return $stmt;
    }

    /**
     * Функция INSERT вставляет данные в БД
     * @access public
     * @param string $table название таблицы
     * @param array $array Асоциативный массив array('имя колонки'=>'вставляемое значение')
     * @return boolean Возвращает true или false
     */
    public function insert($table, $array) {

        $values = array();
        $data = '';

        if (is_int($table) && $table == null)
            exit("Некорректное название  имени таблицы");
        if (!is_array($array) && empty($array))
            exit("Некорректные данные. Данные следует передавать в массиве.");

        foreach ($array as $column => $value) {
            $data .= "`{$column}`=:{$column}, ";
            $values[$column] = $value;
        }
        $data = trim(trim($data), ',');

        $table = $this->tablePref . $table;

        $query = "INSERT INTO `{$table}` SET {$data} ";

        $stmt = $this->db->prepare($query);

        return $stmt->execute($values);
    }

    /**
     * Функция DELETE удаляет данные в БД
     * @access public
     * @param string $table название таблицы
     * @param array $where Асоциативный массив array('имя колонки'=>'значение')
     * @param integer $limit Значение для условия LIMIT
     * @return mixed
     */
    public function delete($table, $where = null, $limit = null) {

        $values = array();
        $data = '';

        if (is_array($where)) {

            foreach ($where as $key => $value) {

                //Делаем выборку каждого элемента массива и сохранаем их всех ввиде одной строки "{$key}={$value} AND ..."

                $data .= "`{$key}` = :{$key} AND ";

                $values[$key] = $value;
            }

            $where = trim(trim($data), 'AND');
        } else if (!is_null($where)) {
            exit("Параметры следует передавать в массиве. Error in " . __LINE__);
        }

        $data = trim(trim($data), ',');

        $table = $this->tablePref . $table;

        $where = (is_null($where)) ? null : "WHERE {$where}";

        $limit = ( is_null($limit) ) ? null : "LIMIT {$limit}";

        $query = "DELETE FROM `{$table}` {$where} {$limit}";

        $stmt = $this->db->prepare($query);

        return $stmt->execute($values);
    }

    private function columns($array) {

        $data = null;

        if (is_array($array)) {

            foreach ($array as $item) {

                $data .= $item . ", ";
            }

            $data = trim(trim($data), ',');
        }

        return $data;
    }

    public function fetch($resource) {

        return $resource->fetch(PDO::FETCH_ASSOC);
    }

    public function fetch_all($resource) {

        return $resource->fetchAll(PDO::FETCH_ASSOC);
    }

    public function __destruct() {
    }
}