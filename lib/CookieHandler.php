<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 03.08.2017
 * Time: 15:17
 */

namespace lib;

use helpers\ConfigHelper;

class CookieHandler {
    public static function set($data, $nameP = null, $timeP = null, $pathP = null) {
        $name = ConfigHelper::get('cookie', 'name');
        $time = ConfigHelper::get('cookie', 'time');
        $hash = ConfigHelper::get('cookie', 'hash');
        $path = '/';

        $timeP = (is_null($timeP)) ? $time : $timeP;
        $pathP = (is_null($pathP)) ? $path : $pathP;
        $value = base64_encode($data) . $hash;

        setcookie($name.'_'.$nameP, $value, time() + $timeP, $pathP);
    }

    public static function get($nameP) {
        $name = ConfigHelper::get('cookie', 'name');
        $hash = ConfigHelper::get('cookie', 'hash');

        if (isset($_COOKIE[$name.'_'.$nameP])) {
            return strip_tags(base64_decode(rtrim($_COOKIE[name.'_'.$nameP], $hash)));
        }

        return false;
    }

    public static function getCoordinates()
    {
        if (!isset($_COOKIE['coord_x'])  || !isset($_COOKIE['coord_y']))
            return ['x' => 0, 'y' => 0];

        $locationX = filter_var($_COOKIE['coord_x'], FILTER_VALIDATE_FLOAT);
        $locationY = filter_var($_COOKIE['coord_y'], FILTER_VALIDATE_FLOAT);

        return [
            'x' => (($locationX) ? $locationX : 0),
            'y' => (($locationY) ? $locationY : 0)
        ];
    }

    public static function destroy($nameP) {
        $name = ConfigHelper::get('cookie', 'name');
        $path = '/';

        return setcookie($name.'_'.$nameP, '', time() - 3600, $path);
    }

}