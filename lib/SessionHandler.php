<?php

namespace lib;

use helpers\ConfigHelper;

class SessionHandler
{
    public static function set($data, $name = null)
    {
        $sessionName = ConfigHelper::get('session', 'name');

        $_SESSION[$sessionName . '_' . $name] = base64_encode($data);
        return true;
    }

    public static function get($name)
    {
        $sessionName = ConfigHelper::get('session', 'name');

        return isset($_SESSION[$sessionName . '_' . $name])
            ? base64_decode($_SESSION[$sessionName . '_' . $name])
            : false;
    }

    public static function destroy($name)
    {
        $sessionName = ConfigHelper::get('session', 'name');

        if (isset($_SESSION[$sessionName . '_' . $name])) {
            unset($_SESSION[$sessionName . '_' . $name]);
            return true;
        }
        return false;
    }
}