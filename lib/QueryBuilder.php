<?php

use core\Config;


//Unused now, just a prototype
class QueryBuilder
{
    const SELECT_ORDER_DESC = 'DESC';
    const SELECT_ORDER_ASC = 'ASC';

    private $tablePref;
    private $sqlString;
    private $dbInterface;

    private static $instance = null;

    public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function __construct()
    {
        $this->tablePref = Config::get('db', 'pref');

        $host   = Config::get('db', 'host');
        $port   = Config::get('db', 'port');
        $dbName = Config::get('db', 'name');
        $user   = Config::get('db', 'user');
        $pass   = Config::get('db', 'pass');

        try {
            $this->dbInterface = new PDO("mysql:host=$host;port=$port;dbname=$dbName;", $user, $pass);

            $this->dbInterface->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbInterface->exec('SET NAMES utf8');
        } catch (PDOException $exception) {
            exit("Нет соединения с базой данных");
        }

        $timezone = Config::get('app', 'timezone');
        $this->dbInterface->exec("SET time_zone = $timezone");
    }

    public function select($table, array $fields = null, $onlyUnique = false)
    {
        $this->sqlString .= "SELECT ";

        if ($onlyUnique) {
            $this->sqlString .= "DISTINCT ";
        }

        if ($fields == null) {
            $this->sqlString .= '*';
        } else {
            $lastField = array_pop($fields);

            foreach ($fields as $index => $item) {
                $this->sqlString .= $item . ', ';
            }

            $this->sqlString .= $lastField;
        }

        $this->sqlString .= " FROM {$table} ";

        return $this;
    }

    public function insert($table, array $fields, array $values)
    {
        $this->sqlString .= "INSERT INTO {$table} ( ";

        $lastField = array_pop($fields);
        foreach ($fields as $field) {
            $this->sqlString .= "{$field}, ";
        }
        $this->sqlString .= $lastField . ') VALUES (';

        $lastValue = array_pop($values);
        foreach ($values as $value) {
            $this->sqlString .= "'{$value}', ";
        }
        $this->sqlString .= "'{$lastValue}'";

        return $this;
    }

    public function update($table, array $columns, array $values)
    {
        if (count($columns) != count($values))
            throw new InvalidArgumentException('array columns and array values should be quantitatively equal');

        $this->sqlString .= "UPDATE {$table} SET";

        $lastColumn = array_pop($columns);
        $lastValue = array_pop($values);

        foreach ($columns as $index => $column) {
            $this->sqlString .= "{$column} = {$values[$index]}, ";
        }
        $this->sqlString .= "{$lastColumn} = {$lastValue} ";

        return $this;
    }

    //[field, op, value] => WHERE field (=, >, <, is, ...) value(could be null)
    //Только один массив с тремя параметрами
    public function where(array $condition)
    {
        $this->sqlString .= "WHERE {$condition[0]} {$condition[1]} {$condition[2]}";
        return $this;
    }

    //[field, op, value] => WHERE field (=, >, <, is, ...) value(could be null)
    //Только один массив с тремя параметрами
    public function andWhere(array $condition)
    {
        $this->sqlString .= " AND {$condition[0]} {$condition[1]} {$condition[2]}";
        return $this;
    }

    public function orderBy(array $conditions, $order = self::SELECT_ORDER_DESC)
    {
        $this->sqlString .= "ORDER BY ";

        $conditionsCount = count($conditions);
        foreach ($conditions as $index => $condition) {
            $this->sqlString .= $condition;
            if ($index != $conditionsCount - 1) {
                $this->sqlString .= ', ';
            }
        }

        $this->sqlString .= " {$order}";

        return $this;
    }

    public function limit($number)
    {
        $this->sqlString .= "LIMIT {$number} ";
        return $this;
    }

    public function execute()
    {
        return $this->dbInterface->prepare($this->sqlString)->execute();
    }

    public function one()
    {
        return $this->endBuild()->fetch(PDO::FETCH_ASSOC);
    }

    public function all()
    {
        return $this->endBuild()->fetchAll(PDO::FETCH_ASSOC);
    }

    private function endBuild()
    {
        $this->sqlString .= ';';

        $stmt = $this->dbInterface->prepare($this->sqlString);
        $stmt->execute();
        return $stmt;
    }
}