<?php

namespace lib;

use helpers\ConfigHelper;

class MailHandler {

    private $mail = null;

    public function __construct($mail = null) {
        $this->mail = $mail;
    }

    public function extendedMail($subject, $text, $from = null, $files = null) {
        if (is_null($this->mail))
            $this->mail = ConfigHelper::get('app', 'support');

        $un = strtoupper(uniqid(time()));

        $headers = "From: $from\n";
        if (is_null($from)) {
            $headers = "From: " . (ConfigHelper::get('app', 'admin')) . "\n";
        }

        $headers .= "To: $this->mail\n";
        $headers .= "Subject: $subject\n";
        $headers .= "X-Mailer: PHPMail Tool\n";
        $headers .= "Reply-To: $from\n";
        $headers .= "Mime-Version: 1.0\n";
        $headers .= "Content-Type:multipart/mixed;";
        $headers .= "boundary=\"----------" . $un . "\"\n\n";

        $body = "------------" . $un . "\nContent-Type:text/html;\n";
        $body .= "Content-Transfer-Encoding: 8bit\n\n";
        $body .= "$text\n\n";

        if (!is_null($files) && is_array($files)) {
            foreach ($files as $file) {
                $body .= "------------" . $un . "\n";
                $body .= "Content-Type: " . $file['type'] . ";";
                $body .= "name=\"" . basename($file['name']) . "\"\n";
                $body .= "Content-Transfer-Encoding:base64\n";
                $body .= "Content-Disposition:attachment;";
                $body .= "filename=\"" . basename($file['name']) . "\"\n\n";
                $body .= chunk_split(base64_encode(file_get_contents($file['url']))) . "\n";
            }
        }
        $body .= "------------" . $un . "--";

        return mail($this->mail, $subject, $body, $headers);
    }

    public function mailToUser($subject, $text, $from = null) {

        if (is_null($this->mail))
            $this->mail = ConfigHelper::get('app', 'support');
        if (is_null($from))
            $from = ConfigHelper::get('app', 'from_mail');

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "To: " . $this->mail . "\r\n";
        if (is_null($from)) {
            $headers .= "From: " . (ConfigHelper::get('app', 'site_name')) . " <" . (ConfigHelper::get('app', 'admin') ) . ">\r\n";
        } else {
            $headers .= "From: " . $from . " <" . $from . ">\r\n";
        }
        $body = '
            <html>
            <head>
              <title>' . $subject . '</title>
            </head>
            <body>
            <p>
                ' . $text . '
            </p>
            <p>С уважением, администрация сайта '.(ConfigHelper::get('app', 'site_name')).'"</p>
            </body>
            </html>';

        return mail($this->mail, $subject, $body, $headers);
    }

    public function setReceiver($mail) {
        if (!empty($mail))
            $this->mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
    }

}