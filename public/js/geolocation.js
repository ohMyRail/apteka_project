/**
 * Created by Раиль on 16.09.2017.
 */
$(document).ready(function () {
    ymaps.ready(init);
});

function init() {
    // Данные о местоположении, определённом по IP
    var geolocation = ymaps.geolocation;
    var coords = [geolocation.latitude, geolocation.longitude];
    var myMap = new ymaps.Map('map', {
            center: coords,
            zoom: 13
        });

    myMap.controls.add('zoomControl', { left: 5, top: 5 });
    myMap.geoObjects.add(new ymaps.Placemark(coords, { balloonContent: 'Вы здесь!' }));

    setCoordinatesInCookie(geolocation.latitude, geolocation.longitude);
    setDataInMap(myMap);
}

function setCoordinatesInCookie(latitude, longitude) {
    setCookie('coord_x', latitude);
    setCookie('coord_y', longitude);
}

function setDataInMap(myMap) {
    var mapInfo = $('#map-info');
    if (mapInfo.length !== 0) {
        var mapData = mapInfo.find(".map-data");
        mapData.each(function () {
            var element = $(this);
            myMap.geoObjects.add(new ymaps.Placemark([element.data('x'), element.data('y')], {
                balloonContentHeader: element.data('header'),
                balloonContentBody: element.data('center')
            }));
        });
    }
}