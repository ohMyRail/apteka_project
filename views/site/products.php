<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 16.09.2017
 * Time: 16:28
 * @var $data
 */
use helpers\HtmlHelper;

?>

<h3>Все лекарства</h3>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Название</th>
                <th>Категория</th>
                <th class="col-md-2">Цены</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['products'] as $product): ?>
                <tr>
                    <td><a href="/product/<?= $product['id'] ?>"><?= $product['name'] ?></a></td>
                    <td><?= $product['cat_name'] ?></td>
                    <td class="col-md-2"><?= (!is_null($product['min_price']) && $product['max_price']) ? $product['min_price'] . ' - ' . $product['max_price'] : 'нет информации о ценах' ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?= HtmlHelper::pagination(null, $data['current_offset'], $data['offsets_count']) ?>

    </div>
</div>