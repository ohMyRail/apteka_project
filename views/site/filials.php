<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 16.09.2017
 * Time: 17:50
 * @var $data
 */
use helpers\HtmlHelper;

?>
<div class="row">
    <div class="col-md-8">
        <h4>Ближайшие аптеки: </h4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Компания</th>
                <th>Адрес</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['filials'] as $filial): ?>
                <tr>
                    <td><?= $filial['company_name']; ?></td>
                    <td><?= HtmlHelper::filialAddress($filial['street'], $filial['building']); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?= HtmlHelper::pagination(null, $data['current_offset'], $data['offsets_count']) ?>

    </div>


    <div id="map" style="width: 100%; height: 350px; padding: 0; margin: 0;"></div>
</div>
