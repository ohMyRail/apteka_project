<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 16.09.2017
 * Time: 16:10
 * @var $data
 */
use helpers\HtmlHelper;

?>

<h3>категория: <i><?= $data['cat_name'] ?></i></h3>
<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Название</th>
                <th>Описание</th>
                <th class="col-md-2">Цены</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['products'] as $product): ?>
                <tr>
                    <td><a href="/product/<?= $product['id'] ?>"><?= $product['name'] ?></a></td>
                    <td><?= HtmlHelper::description($product['description']) ?></td>
                    <td class="col-md-2"><?= (!is_null($product['min_price']) && $product['max_price']) ? $product['min_price'] . ' - ' . $product['max_price'] : 'нет информации о ценах' ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?= HtmlHelper::pagination(['id' => $data['cat_id']], $data['current_offset'], $data['offsets_count']) ?>

    </div>
</div>
