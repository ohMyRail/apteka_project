<h2>Более 10000 лекарств и 70 аптек Казани на одном сайте</h2>
<div class="row">
    <form class="row col-md-10" method="get" action="/search">
        <div class="form-group col-md-8">
            <input type="text" name="keyword" class="form-control" placeholder="Введите название лекарства, аптеки, категории">
        </div>
        <div class="col-md-4">
            <input type="submit" name="submit" value="Найти" class="btn btn-default">
        </div>
    </form>
</div>

<div class="row">
    <div id="map" style="width: 100%; height: 350px; padding: 0; margin: 0;"></div>
</div>
