<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 16.09.2017
 * Time: 2:59
 * @var $data
 */
use helpers\HtmlHelper;

?>
<div class="row">
    <div class="col-md-4">
        <ul class="list-group">
            <li class="list-group-item">
                <i>Название: </i>
                <?= $data['product']['name'] ?>
            </li>
            <li class="list-group-item">
                <i>Описание: </i>
                <?= $data['product']['description'] ?>
            </li>
            <li class="list-group-item">
                <i>Категория: </i>
                <a href="/category?id=<?= $data['product']['id_category']; ?>">
                    <?= $data['product']['cat_name'] ?>
                </a>
            </li>
        </ul>
    </div>

    <div class="col-md-8">
        <h4><?= $data['product']['name'] ?> можно купить в ближайших аптеках: </h4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Компания</th>
                <th>Адрес</th>
                <th>Цена</th>
                <th>В наличии</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['filials'] as $filial): ?>
                <tr>
                    <td><?= $filial['company_name']; ?></td>
                    <td><?= HtmlHelper::filialAddress($filial['street'], $filial['building']); ?></td>
                    <td><?= $filial['price']; ?>р.</td>
                    <td><?= ($filial['count'] != 0) ? $filial['count'] : 'нет'; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <div id="map-info" hidden>
        <?php foreach ($data['filials'] as $filial): ?>
            <div class="map-data" data-x="<?= $filial['coord_x']; ?>" data-y="<?= $filial['coord_y']; ?>" data-header="<?= $filial['company_name']; ?>" data-center="<?= HtmlHelper::filialAddress($filial['street'], $filial['building']); ?>"></div>
        <?php endforeach; ?>
    </div>

    <div id="map" style="width: 100%; height: 350px; padding: 0; margin: 0;"></div>
</div>
