<?php

/** @var array $data
 *  @var $viewFile
 */

use helpers\ConfigHelper;
use helpers\DateTimeHelper;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php ConfigHelper::publicUrl(); ?>css/bootstrap.min.css" rel="stylesheet">

    <script src="<?php ConfigHelper::publicUrl(); ?>js/cookies.js" type="text/javascript"></script>
    <script src="<?php ConfigHelper::publicUrl(); ?>js/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="//api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script src="<?php ConfigHelper::publicUrl(); ?>js/geolocation.js" type="text/javascript"></script>

    <title>Аптеки.ру - найди лучшее предложение по лекарствам</title>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Аптеки.ру</a>
            <ul class="nav navbar-nav">
                <li><a href="/products">Лекарства</a></li>
                <li><a href="/filials">Адреса</a></li>
                <li><a href="/companies">Аптеки</a></li>
                <li><a href="/about">О проекте</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container body-content">
    <?php require_once $viewFile; ?>
    <hr />
    <footer>
        <div class="">
            <p>Аптеки.ру - <?= DateTimeHelper::getYear(new DateTime()); ?> г.</p>
        </div>
    </footer>
</div>
</body>
</html>
