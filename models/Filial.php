<?php

namespace models;

use core\Config;
use core\Model;
use DateInterval;
use DateTime;
use helpers\ArrayHelper;
use helpers\ConfigHelper;
use helpers\DateTimeHelper;
use helpers\LogHelper;
use function PHPSTORM_META\type;

class Filial extends Model
{
    protected static $table = 'filial_data';

    public static function getFilialDataByProduct($productID, $coordinates)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT filial.*, comp.name as company_name, p_c.price, p_f.count "
            . "FROM {$pref}filial_data AS filial "
            . "JOIN {$pref}product_filial AS p_f ON filial.id = p_f.id_filial "
            . "JOIN {$pref}product_company AS p_c ON (filial.id_company = p_c.id_company AND p_f.id_product = p_c.id_product) "
            . "JOIN {$pref}company_data AS comp ON comp.id = filial.id_company "
            . "WHERE p_f.id_product = {$productID} ORDER BY (filial.coord_y - {$coordinates['y']}) * (filial.coord_y - {$coordinates['y']}) + "
            . "(filial.coord_x - {$coordinates['x']}) * (filial.coord_x - {$coordinates['x']})";

//        LogHelper::log($query);

        return self::fetchAll(self::buildQuery($query));
    }

    public static function getAllDataByParams($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT filial.*, comp.name as company_name "
            . "FROM {$pref}filial_data AS filial "
            . "JOIN {$pref}company_data AS comp ON comp.id = filial.id_company "
            . "ORDER BY (filial.coord_y - {$params['y']}) * (filial.coord_y - {$params['y']}) + "
            . "(filial.coord_x - {$params['x']}) * (filial.coord_x - {$params['x']}) LIMIT {$params['offset']}, {$params['limit']}";

//        LogHelper::log($query);

        return self::fetchAll(self::buildQuery($query));
    }

    public static function getAllDataCount()
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT COUNT(id) as count FROM {$pref}filial_data";

//        LogHelper::log($query);

        return self::fetch(self::buildQuery($query))['count'];
    }

    public static function getCompanyFilials($companyId)
    {
        return self::selectMany(self::$table, null, [
           'id_company' => $companyId
        ]);
    }
}