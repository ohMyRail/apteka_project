<?php

namespace models;

use helpers\ConfigHelper;
use helpers\ArrayHelper;
use core\Model;

class Category extends Model
{
    protected static $table = 'category_data';

    //Получает все доступные категории, отсортированные в порядке sort_value
    public static function getAllCategories()
    {
        return self::selectMany(self::$table);
    }

    public static function insertData($name)
    {
        return self::insert(self::$table, [
            'name' => $name,
            ]);
    }

    public static function getDataByName($name)
    {
        return self::selectOne(self::$table, null, [
            'name' => $name
        ]);
    }

    public static function isExistByName($name)
    {
        return (!empty(self::getDataByName($name)));
    }
}