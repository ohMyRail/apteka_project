<?php

namespace models;

use helpers\ConfigHelper;
use core\Model;
use DateTime;
use helpers\GeneratorHelper;
use lib\SessionHandler;

//confirmation_data here as well
class User extends Model
{
    protected static $table = 'user_data';

    public static function getDataByAttributes($condition)
    {
        return self::selectOne('user_data', null, $condition);
    }

    public static function getUsersByType($type)
    {
        return self::selectMany('user_data', null, ['type' => $type]);
    }

    public static function getUserByEmail($email, $needle = null)
    {
        return self::selectOne('user_data', $needle, ['email' => $email]);
    }

    public static function getUserByPhone($phone, $needle = null)
    {
        return self::selectOne('user_data', $needle, ['phone' => $phone]);
    }

    //returns user data if session array has the value or null otherwise
    public static function getUserDataFromSession()
    {
        $userID = SessionHandler::get('user');
        if ($userID == false) {
            return null;
        }
        return self::getDataByAttributes(['id' => $userID]);
    }

    public static function getBalance($userId)
    {
        /*
        $pref = $this->getPrefix();

        $query = "SELECT sum(`sum`) as sum FROM {$pref}accounting_data WHERE status=1 AND ac_type=:type AND user=:user LIMIT 1";

        $resource = $this->_db->lowQuery($query, [
            'user' => $userId,
            'type' =>'in'
        ]);
        $result = $this->_db->fetch($resource);

        $in = $result['sum'];

        $resource = $this->_db->lowQuery($query, [
            'user' => $userId,
            'type' =>'out'
        ]);
        $result = $this->_db->fetch($resource);

        $out = $result['sum'];

        return $in - $out;*/
    }

    public static function checkUserPhone($phone)
    {
        if (self::isExistByPhone($phone) || Company::isExistByPhone($phone) || Filial::isExistByPhone($phone))
            return false;

        return true;
    }

    //Проверяет есть ли в базе юзер с переданным номером телефона
    public static function isExistByPhone($phone)
    {
        $userID = self::selectOne('user_data', ['id'], ['phone' => $phone]);

        return !empty($userID);
    }

    //Проверяет есть ли в базе юзер с переданной почтой
    public static function isEmailExist($email)
    {
        $userID = self::selectOne('user_data', ['id'], ['email' => $email]);

        return !empty($userID);
    }

    public static function saveUserByPhone($phone, $password, $userType)
    {
        return self::saveNewUser(['phone' => $phone, 'password' => $password, 'userType' => $userType]);
    }

    public static function saveUserByEmailAndPhone($email, $phone, $password, $userType)
    {
        return self::saveNewUser(['email' => $email, 'phone' => $phone, 'password' => $password, 'userType' => $userType]);
    }

    public static function saveNewUser($params = ['email' => "", 'phone' => "", 'password' => "", 'userType' => ""])
    {
        $userType = $params['userType'];

        $insert = [
            'name' => ($userType == 'seller') ? 'Компания' : 'Пользователь',
            'pass' => md5($params['password'] . ConfigHelper::get('secure', 'hashKey')),
            'referal' => GeneratorHelper::generateReferURL(),
            'type' => ($userType == 'seller') ? 4 : 5,
            'status' => 0
        ];

        $insert['phone'] = isset($params['phone']) ? $params['phone'] : null;
        $insert['email'] = isset($params['email']) ? $params['email'] : null;

        if ($insert['phone'] == null)
            exit("PHONE CAN'T BE NULL");

        return self::insert('user_data', $insert);
    }

    public static function saveUserConfirmationCodeByEmail($email)
    {
        return self::saveUserConfirmationCode(['email' => $email]);
    }

    public static function saveUserConfirmationCodeByPhone($userID, $phone, $code)
    {
        return self::saveUserConfirmationCode(['userID' => $userID, 'phone' => $phone, 'code' => $code]);
    }

    public static function saveUserConfirmationCode($params = ['userID' => null, 'email' => null, 'phone' => null, 'code' => null])
    {
        $type = null;
        $data = null;

        $userID = isset($params['userID'])  ? $params['userID']   : null;
        $email  = isset($params['email'])   ? $params['email']    : null;
        $phone  = isset($params['phone'])   ? $params['phone']    : null;

        if (!isset($params['code'])) {
            exit('FATAL: THERE IS NO CONFIRMATION CODE');
        }
        $code = GeneratorHelper::generateConfirmCode($userID, $params['code']);

        if ($email == null and $phone == null) {
            return false;
        }

        if ($email != null) {
            $userID = self::getUserByEmail($email, ['id']);
            $type = 0;
            $data = $email;
        }
        if ($phone != null) {
            $userID = self::getUserByPhone($phone, ['id']);
            $type = 1;
            $data = $phone;
        }

        $insert = [
            'id_user' => $userID,
            'hash' => $code,
            'status' => 0,
            'date' => date('Y-m-d H:i:s', time()),
            'type' => $type,
            'data' => $data
        ];

        return self::insert('confirmation_data', $insert);
    }

    public static function getConfirmHash($params = ['email' => null, 'phone' => null])
    {
        $userID = null;
        if (!isset($params['email']) && !isset($params['phone'])) {
            return null;
        }
        if (isset($params['email'])) {
            $userID = self::getUserByEmail($params['email'], ['id']);
        }
        if (isset($params['phone'])) {
            $userID = self::getUserByPhone($params['phone'], ['id']);
        }

        return self::selectOne('confirmation_data', ['hash'], ['id_user' => $userID]) ['hash'];
    }

    public static function isUserExistsInConfirmData($userID, $phone)
    {
        $user = self::selectOne('confirmation_data', ['id_user'], ['data' => $phone, 'id_user' => $userID]);
        return !empty($user);
    }

    public static function isExistUserWithThisPhoneInConfirmData($phone)
    {
        $userID = User::getUserByPhone($phone, ['id']);
        $user = self::selectOne('confirmation_data', ['user'], ['data' => $phone, 'id_user' => $userID]);
        return !empty($user);
    }

    public static function updateUserSmsConfirmHash($userID, $phone, $code)
    {
        $insert = [
            'hash' => GeneratorHelper::generateConfirmCode($userID, $code),
            'status' => 0,
            'date' => date('Y-m-d H:i:s', time()),
        ];

        return self::update('confirmation_data', $insert, ['data' => $phone, 'id_user' => $userID]);
    }

    public static function getUserCountByType($type = null)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT count(*) as count FROM {$pref}user_data WHERE status=1";
        if ($type !== null) {
            $query .= " AND type={$type}";
        }

        $resource = self::buildQuery($query);

        return self::fetch($resource)['count'];
    }

    public static function isCorrectCode($code, $userID)
    {
        $hash = GeneratorHelper::generateConfirmCode($userID, $code);

        $confirmationData = self::selectOne('confirmation_data', null, ['id_user' => $userID, 'hash' => $hash]);
        return !empty($confirmationData);
    }

    public static function setSmsConfirmStatus($userID)
    {
        return self::update('confirmation_data', ['status' => 1], ['id_user' => $userID, 'type' => 1]);
    }

    //possible mistake in sql query, because ya pisal ego, ya tupaya cunt
    public static function getSocialNetworkName($userID)
    {
        $pref = ConfigHelper::getPrefix();
        $query = "SELECT t1.name as name FROM {$pref}social_network_data as t1 "
            . "LEFT JOIN {pref}social_network_user as t2 on t1.id_network=t2.id_network "
            . "WHERE t2.id_user={$userID}";

        return self::fetch(self::buildQuery($query)) ['name'];
    }

    public static function isRegisteredThroughSocialNetwork($userID)
    {

    }

    public static function updateFromInitial($insert, $where)
    {
        self::update('user_data', $insert, $where);
    }

}