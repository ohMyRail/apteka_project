<?php

namespace models;

use helpers\ConfigHelper;
use core\Model;
use helpers\LogHelper;

class Product extends Model
{
    protected static $table = 'product_data';

    public static function getAllDataCount()
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT COUNT(id) as count FROM {$pref}product_data";

//        LogHelper::log($query);

        return self::fetch(self::buildQuery($query))['count'];
    }

    public static function getCategoryDataCount($categoryId)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT COUNT(id) as count FROM {$pref}product_data WHERE id_category=:id_category";

//        LogHelper::log($query);

        return self::fetch(self::buildQuery($query, ['id_category' => $categoryId]))['count'];
    }

    public static function searchData($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT prod.*, cat.name as cat_name FROM {$pref}product_data as prod LEFT JOIN {$pref}category_data AS cat ON (prod.id_category=cat.id) WHERE prod.name LIKE '%{$params['keyword']}%' OR prod.description LIKE '%{$params['keyword']}%' LIMIT {$params['offset']}, {$params['limit']}";

        LogHelper::log($query);

        return self::fetchAll(self::buildQuery($query));
    }

    public static function getNumberOfSearchingData($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT COUNT(prod.id) as count FROM {$pref}product_data as prod LEFT JOIN {$pref}category_data AS cat ON (prod.id_category=cat.id) WHERE prod.name LIKE '%{$params['keyword']}%' OR prod.description LIKE '%{$params['keyword']}%'";

        return self::fetch(self::buildQuery($query))['count'];
    }

    public static function getMinPrice($productId)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT MIN(price) as min_price FROM {$pref}product_company WHERE id_product=:id_product";

        return self::fetch(self::buildQuery($query, ['id_product' => $productId]))['min_price'];
    }

    public static function getMaxPrice($productId)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT MAX(price) as max_price FROM {$pref}product_company WHERE id_product=:id_product";

        return self::fetch(self::buildQuery($query, ['id_product' => $productId]))['max_price'];
    }

    public static function getAllDataByParams($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT prod.*, cat.name as cat_name FROM {$pref}product_data as prod LEFT JOIN {$pref}category_data AS cat ON (prod.id_category=cat.id) LIMIT {$params['offset']}, {$params['limit']}";

        return self::fetchAll(self::buildQuery($query));
    }

    public static function getDataByCategory($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT * FROM {$pref}product_data WHERE id_category=:id_category LIMIT {$params['offset']}, {$params['limit']}";

//        LogHelper::log($query);

        return self::fetchAll(self::buildQuery($query, ['id_category' => $params['id_category']]));
    }

    public static function getDataByName($name)
    {
        return self::selectOne(self::$table, null, [
            'name' => $name
        ]);
    }

    public static function isExistByName($name)
    {
        return !(empty(self::getDataByName($name)));
    }

    public static function insertData($params)
    {
        return self::insert(self::$table, $params);
    }

    public static function getProductCompany($companyId, $productId)
    {
        return self::selectOne('product_company', null, [
            'id_company' => $companyId,
            'id_product' => $productId
         ]);
    }

    public static function isProductCompanyExist($companyId, $productId)
    {
        return (!empty(self::getProductCompany($companyId, $productId)));
    }

    public static function insertProductCompany($companyId, $productId, $price)
    {
        return self::insert('product_company', [
            'id_company' => $companyId,
            'id_product' => $productId,
            'price' => $price
        ]);
    }

    public static function getProductFilial($filialId, $productId)
    {
        return self::selectOne('product_filial', null, [
            'id_filial' => $filialId,
            'id_product' => $productId
        ]);
    }

    public static function isProductFilialExist($filialId, $productId)
    {
        return (!empty(self::getProductFilial($filialId, $productId)));
    }

    public static function insertProductFilial($filialId, $productId, $count)
    {
        return self::insert('product_filial', [
            'id_filial' => $filialId,
            'id_product' => $productId,
            'count' => $count
        ]);
    }

}