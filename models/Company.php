<?php

namespace models;

use core\Model;
use DateTime;
use helpers\ConfigHelper;
use helpers\ArrayHelper;
use helpers\DateTimeHelper;
use helpers\LogHelper;
use helpers\ImageHelper;
use helpers\TranslitHelper;

class Company extends Model
{
    protected static $table = 'company_data';

    public static function getAllDataByParams($params)
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT filial.street, filial.building, comp.name as company_name, comp.id, comp.site, comp.phone, "
            . "((filial.coord_y - {$params['y']}) * (filial.coord_y - {$params['y']}) + "
            . "(filial.coord_x - {$params['x']}) * (filial.coord_x - {$params['x']})) as distance "
            . "FROM {$pref}company_data AS comp "
            . "JOIN {$pref}filial_data AS filial ON comp.id = filial.id_company "
            . "WHERE filial.id = (SELECT f2.id FROM {$pref}filial_data AS f2 WHERE f2.id_company = comp.id "
                . "ORDER BY ((filial.coord_y - {$params['y']}) * (filial.coord_y - {$params['y']}) +  (filial.coord_x - {$params['x']}) * (filial.coord_x - {$params['x']})) LIMIT 1) "
            . "GROUP BY comp.id "
            . "ORDER BY distance "
            . "LIMIT {$params['offset']}, {$params['limit']} ";
        LogHelper::log($query);

        return self::fetchAll(self::buildQuery($query));
    }

    public static function getAllDataCount()
    {
        $pref = ConfigHelper::getPrefix();

        $query = "SELECT COUNT(id) as count FROM {$pref}company_data";

//        LogHelper::log($query);

        return self::fetch(self::buildQuery($query))['count'];
    }
}