<?php

namespace autoload;


use Exception;

class Autoload
{
    private $fileExtension = '.php';
    private $namespace;
    private $includePath;
    private $namespaceSeparator = '\\';

    public function __construct($namespace = null, $includePath = null)
    {
        $this->namespace = $namespace;
        $this->includePath = $includePath;
    }

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    private function loadClass($className)
    {
        if (null === $this->namespace || $this->namespace . $this->namespaceSeparator === substr($className, 0, strlen($this->namespace . $this->namespaceSeparator))) {
            $fileName = '';
            $namespace = '';
            if (($lastNsPos = strripos($className, $this->namespaceSeparator)) !== false) {
                $namespace = substr($className, 0, $lastNsPos);
                $className = substr($className, $lastNsPos + 1);
                $fileName = str_replace($this->namespaceSeparator, DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
            }
            $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . $this->fileExtension;
            require_once ($this->includePath !== null ? $this->includePath . DIRECTORY_SEPARATOR : '') . $fileName;

        } else {
            throw new Exception('check your filenames and classnames');
        }
    }
}