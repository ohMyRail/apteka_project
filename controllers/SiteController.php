<?php

namespace controllers;

use core\Controller;
use DateTime;
use helpers\ArrayHelper;
use helpers\ConfigHelper;
use helpers\DateTimeHelper;
use helpers\ImageHelper;
use helpers\MessageHelper;
use helpers\TranslitHelper;
use helpers\ValidateHelper;
use helpers\CountingHelper;
use lib\CookieHandler;
use lib\SessionHandler;
use models\Category;
use models\Company;
use models\Filial;
use models\Product;
use models\Specialist;
use models\StaticInfo;
use models\User;

class SiteController extends Controller
{
    protected $layout = 'layout';

    public function actionIndex()
    {
        return $this->render('homepage');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSearch($keyword, $offset = 1)
    {
        $params = ValidateHelper::validateSearchParams();

        $productsData = Product::searchData($params);

        foreach ($productsData as $key => $value) {
            $productsData[$key]['min_price'] = Product::getMinPrice($productsData[$key]['id']);
            $productsData[$key]['max_price'] = Product::getMaxPrice($productsData[$key]['id']);
        }

        $this->render('search', [
            'keyword' => $params['keyword'],
            'products' => $productsData,
            'current_offset' => $offset,
            'offsets_count' => CountingHelper::getNumberOfOffsets($params['limit'], Product::getNumberOfSearchingData($params))
        ]);
    }

    public function actionProduct($id)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $productData = Product::getData($id);

        if (empty($productData))
            $this->renderNotFound();

        $productData['logo'] = ImageHelper::getProductLogo($id);

        $productData['cat_name'] = Category::getData($productData['id_category'], ['name']);

        $coordinates = CookieHandler::getCoordinates();

        return $this->render('product', [
            'product' => $productData,
            'filials' => Filial::getFilialDataByProduct($id, $coordinates)
        ]);
    }

    public function actionFilials($offset = 1)
    {
        $params = ValidateHelper::validateLimitParams($offset);

        $filialData = Filial::getAllDataByParams(ArrayHelper::merge($params, CookieHandler::getCoordinates()));

        return $this->render('filials', [
            'filials' => $filialData,
            'current_offset' => $offset,
            'offsets_count' => CountingHelper::getNumberOfOffsets($params['limit'], Filial::getAllDataCount())
        ]);
    }

    public function actionCompanies($offset = 1)
    {
        $params = ValidateHelper::validateLimitParams($offset);

        $companiesData = Company::getAllDataByParams(ArrayHelper::merge($params, CookieHandler::getCoordinates()));

        return $this->render('companies', [
            'companies' => $companiesData,
            'current_offset' => $offset,
            'offsets_count' => CountingHelper::getNumberOfOffsets($params['limit'], Company::getAllDataCount())
        ]);
    }

    public function actionProducts($offset = 1)
    {
        $params = ValidateHelper::validateLimitParams($offset);

        $productsData = Product::getAllDataByParams($params);

        foreach ($productsData as $key => $value) {
            $productsData[$key]['min_price'] = Product::getMinPrice($productsData[$key]['id']);
            $productsData[$key]['max_price'] = Product::getMaxPrice($productsData[$key]['id']);
        }

        return $this->render('products', [
            'products' => $productsData,
            'current_offset' => $offset,
            'offsets_count' => CountingHelper::getNumberOfOffsets($params['limit'], Product::getAllDataCount())
        ]);
    }

    public function actionCategory($id, $offset = 1)
    {
        $id = filter_var($id, FILTER_SANITIZE_NUMBER_INT);

        $params = ValidateHelper::validateLimitParams($offset);

        $categoryData = Category::getData($id);

        if (empty($categoryData))
            return $this->renderNotFound();

        $productsData = Product::getDataByCategory(ArrayHelper::merge($params, ['id_category' => $id]));

        foreach ($productsData as $key => $value) {
            $productsData[$key]['min_price'] = Product::getMinPrice($productsData[$key]['id']);
            $productsData[$key]['max_price'] = Product::getMaxPrice($productsData[$key]['id']);
        }

        return $this->render('category', [
            'cat_id' => $id,
            'cat_name' => $categoryData['name'],
            'products' => $productsData,
            'current_offset' => $offset,
            'offsets_count' => CountingHelper::getNumberOfOffsets($params['limit'], Product::getCategoryDataCount($id))
        ]);
    }

    public function actionInsertProductCompany()
    {
        $minPrice = 20;
        $maxPrice = 1000;

        $allCompaniesCount = 10;
        $maxProductCount = 100;

        $categories = Category::getAllCategories();

        foreach ($categories as $category)
        {
            $categoryMiddlePrice = mt_rand($minPrice, $maxPrice);

            $products = Product::getDataByCategory($category['id']);

            foreach ($products as $product)
            {
                $companiesCount = mt_rand(0, $allCompaniesCount);

                $i = 0;

                while ($i < $companiesCount)
                {
                    $companyId = (int) mt_rand(1, $allCompaniesCount);

//                    var_dump($companyId, $product['id']);
//                    echo "<br/>";

                    if (!Product::isProductCompanyExist($companyId, $product['id']))
                    {
                        if (Product::insertProductCompany($companyId, $product['id'], mt_rand($categoryMiddlePrice * 0.7, $categoryMiddlePrice * 1.3)))
                        {
                            $filials = Filial::getCompanyFilials($companyId);

                            foreach ($filials as $filial) {
                                if (!Product::isProductFilialExist($filial['id'], $product['id']))
                                    Product::insertProductFilial($filial['id'], $product['id'], mt_rand(0, $maxProductCount));
                            }
                        }
                    }
                    $i++;
                }
            }
        }

        return $this->encodeJSON('yes, good');
    }

    public static function actionInsertCategory()
    {
        $fp = fopen(ROOT . '\category.txt', 'r');
        if ($fp){
            while (!feof($fp)){
                $categoryName = substr(fgets($fp, 999), 0, -2);
                if (!Category::isExistByName($categoryName))
                    Category::insertData($categoryName);
            }

            fclose($fp);
        }
        else echo "Ошибка при открытии файла";

        var_dump('success');
    }

    public static function actionInsertProduct()
    {
        $products = [];
        $counter = 0;
        $size = 3600;

        $categoriesFile = fopen(ROOT . '\category.txt', 'r');
        $productNameFile = fopen(ROOT . '\product_name.txt', 'r');
        $productNameFile2 = fopen(ROOT . '\product_name2.txt', 'r');

        if ($productNameFile2){
            while (!feof($productNameFile2) && $counter < $size){
                $name = substr(fgets($productNameFile2, 999), 0, -2);
                $products[$counter]['name'] = $name;
                $counter++;
            }

            fclose($productNameFile2);
        }
        else echo "Ошибка при открытии файла";

        $counter = 0;

        if ($productNameFile){
            while (!feof($productNameFile) && $counter < $size){
                $description = substr(fgets($productNameFile, 255), 0, -2);
                if (strlen($description) > 10){
                    $products[$counter]['description'] = $description;
                    $counter++;
                }

            }

            fclose($productNameFile);
        }
        else echo "Ошибка при открытии файла";

        $counter = 0;

        if ($categoriesFile){
            while (!feof($categoriesFile) &&  $counter < $size){
                $products[$counter]['category'] = substr(fgets($result = $categoriesFile, 999), 0, -2);
                $counter++;
            }

            fclose($categoriesFile);
        }
        else echo "Ошибка при открытии файла";

        foreach ($products as $key => $product)
        {
            if (!Product::isExistByName($product['name']) && !Product::isExistByName($product['name']) . '©') {
                $categoryData = Category::getDataByName($product['category']);
                if (!empty($categoryData)) {
                    $products[$key]['id_category'] = $categoryData['id'];
                    unset($products[$key]['category']);
                    Product::insertData($products[$key]);
                }
            }
        }
    }
}