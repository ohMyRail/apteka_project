<?php

namespace core;

use ReflectionClass;

class Router
{
    private $url;
    private static $instance;

    private function __construct()
    {
        $this->url = new Url();
    }

    public static function getInstance()
    {
        if (empty(self::$instance))
            self::$instance = new self;

        return self::$instance;
    }

    /*
     * Функция запускает метод checkUrl(), подключает требуемый файл контроллера и запускает action,
     * передавая в него атрибуты;
     *
     * Возможная просадка производительности из-за ReflectionAPI, подумать о возможности увеличения числа контроллеров
     * или об отказе от ReflectionAPI at all
     */
    public function goRouting()
    {
        $this->buildUrl();
        $controller = $this->url->getController();
        $action = $this->url->getAction();
        $requestParameters = $this->url->getRequestParameters();

        $controllerReflection = new ReflectionClass($controller);
        $controllerInstance = $controllerReflection->newInstance();

        if (!$controllerReflection->hasMethod($action)) {
            return require_once ROOT . DS . 'views' . DS . '404.php';
        }

        $controllerMethod = $controllerReflection->getMethod($action);

        $methodParameters = $controllerMethod->getParameters();
        $parameterArray = [];

        foreach ($methodParameters as $methodParameter) {
            if ($this->url->getParameter() != null)
                $parameterArray[] = $this->url->getParameter();

            if (array_key_exists($methodParameter->getName(), $requestParameters))
                $parameterArray[] = $requestParameters[$methodParameter->getName()];
        }

        if ($controllerReflection->hasMethod('checkAuth')) {
            $granted = $controllerReflection->getMethod('checkAuth')->invoke($controllerInstance);

            if (!$granted)
                header('Location: /');
        }

        if ($controllerReflection->hasMethod('beforeAction'))
            $controllerReflection->getMethod('beforeAction')->invoke($controllerInstance);

        $controllerMethod->invokeArgs($controllerInstance, $parameterArray);
    }

    private function buildUrl()
    {
        $uri = $_SERVER['REQUEST_URI'];

        $urlPart = $uri;
        $requestPart = '';

        if ($pos = stripos($uri, '?')) {
            $urlPart = substr($uri, 0, $pos);
            $requestPart = substr($uri, $pos + 1);
        }
        $urlPart = ($str = trim($urlPart, '/')) ? $str : 'index';

        $requestPart = trim($requestPart, '/');

        $requestParameters = [];

        if ($requestPart != '') {
            parse_str($requestPart, $requestParameters);
        }
        $this->url->setRequestParameters($requestParameters);

        $urlPieces = explode('/', $urlPart);

        //Составляет имя возможно существующего файла controllers/{Название контроллера}Controller
        //По сути, если существует, то имя файла является путем контроллера
        //Здесь может быть баг из-за отсутствия абсолютного пути
        $possibleFilename = 'controllers' . '\\' . ucfirst($urlPieces[0]) . 'Controller';

        if (file_exists($possibleFilename . '.php')) {
            $this->url->setController($possibleFilename);
            if (empty($urlPieces[1]))
                $this->url->setAction('index');
            else
                $this->url->setAction($urlPieces[1]);

            if (isset($urlPieces[2]) and !empty($urlPieces[2])) {
                $this->url->setParameter($urlPieces[2]);
            }
        } else {
            $this->url->setController('controllers' . '\\' . 'SiteController');
            $this->url->setAction($urlPieces[0]);
            if (isset($urlPieces[1]) and !empty($urlPieces[1])) {
                $this->url->setParameter($urlPieces[1]);
            }
        }
    }
}

