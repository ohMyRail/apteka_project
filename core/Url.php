<?php

namespace core;

class Url
{
    private $controller;
    private $action;

    //$requestParameters - ассоциативный массив вида [a => value1, b => value2, ...],
    //где a, b.. - это параметры GET запроса, а value1, value2.. - их значения
    private $requestParameters;

    //e.g. /categories/krasota -> krasota is parameter
    private $parameter = null;

    public function getController()
    {
        return $this->controller;
    }

    public function setController($controller)
    {
        $this->controller = $controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = self::transformActionString($action);
    }

    public function setRequestParameters($requestParameters)
    {
        $this->requestParameters = $requestParameters;
    }

    public function getRequestParameters()
    {
        return $this->requestParameters;
    }

    public function getParameter()
    {
        return $this->parameter;
    }

    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    private static function transformActionString($action)
    {
        $actionPieces = explode('-', $action);

        foreach ($actionPieces as $index => $piece) {
            $actionPieces[$index] = ucfirst($piece);
        }

        return 'action' . implode($actionPieces);
    }
}