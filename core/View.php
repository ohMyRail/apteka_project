<?php

namespace core;

class View
{
    public function render($view, $data, $layout)
    {
        $viewFile = ROOT . DS . 'views' . DS . $view . '.php';

        //TODO: Дичь, переделать
        $layoutFile = $layout != null
            ? ROOT . DS . 'views' . DS . 'layouts' . DS . $layout . '.php'
            : $viewFile;

        if (!file_exists($viewFile) or !file_exists($layoutFile))
            exit();
        else
            require_once $layoutFile;


        return false;
    }
}