<?php

namespace core;

use models\User;
use ReflectionClass;

abstract class Controller
{
    const NO_LAYOUT = true;

    private $view;
    protected $layout;
    protected $authorizedUser;

    //Главная часть имени контроллера, вызывающего методы абстрактного контроллера. SiteController -> site
    protected $primaryName;

    public function __construct()
    {
        $this->authorizedUser = User::getUserDataFromSession();
        $this->primaryName = $this->getPrimaryName((new ReflectionClass($this))->getName());
        $this->view = new View();
    }

    protected function render($view, $data = null, $layoutFlag = false)
    {
        if ($data == null) {
            $data = [];
        }
        $data['user'] = $this->authorizedUser;

        $view = $this->primaryName . DS . $view;

        if ($layoutFlag)
            $this->layout = null;

        return $this->view->render($view, $data, $this->layout);
    }

    protected function encodeJSON($data)
    {
        echo json_encode($data);
        return false;
    }

    protected function redirect($url)
    {
        header('Location: ' . $url);
        return false;
    }

    //SiteController -> site, AdminController -> admin
    private function getPrimaryName($controllerName)
    {
        return lcfirst(str_replace('Controller', '', str_replace('controllers\\', '', $controllerName)));
    }

    protected function renderNotFound()
    {
        return require_once ROOT . DS . 'views' . DS . '404.php';
    }
}