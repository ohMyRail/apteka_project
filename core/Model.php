<?php

namespace core;

use lib\DBHandler;
use ReflectionClass;

abstract class Model
{
    protected static $table;

    public static function getData($id, $needle = null)
    {
        return self::selectOne(static::$table, $needle, ['id' => $id]);
    }

    /**
     * @param DBHandler $instance
     * @param string $table
     * @param array $needle
     * @param array $where
     * @param array $orderby
     * @param array $limit
     * @return resource
     *
     * Принимает еще и $instance для того, чтобы не обращаться к DBHandler лишний раз
     */
    private static function select($instance, $table, $needle = array('*'), $where = array(), $orderby = array(), $limit = array())
    {
        return $instance->select($table, $needle, $where, $orderby, $limit);
    }

    protected static function count($table, $where = [], $as = 'count')
    {
        $instance = DBHandler::getInstance();

        $resource = $instance->count($table, $where, $as);

        return $instance->fetch($resource) [$as];
    }

    protected static function selectOne($table, $needle = array('*'), $where = array())
    {
        $instance = DBHandler::getInstance();
        $resource = self::select($instance, $table, $needle, $where);

        $result = $instance->fetch($resource);

        return (count($needle) == 1 and $needle[0] != '*')? $result[$needle[0]] : $result;
    }

    protected static function selectMany($table, $needle = array('*'), $where = array(), $orderby = array(), $limit = array())
    {
        $instance = DBHandler::getInstance();
        $resource = self::select($instance, $table, $needle, $where, $orderby);

        return $instance->fetch_all($resource);
    }

    protected static function update($table, $data, $where = null, $limit = null)
    {
        return DBHandler::getInstance()->update($table, $data, $where, $limit);
    }

    protected static function insert($table, $insertParams)
    {
        return DBHandler::getInstance()->insert($table, $insertParams);
    }

    protected static function delete($table, $where)
    {
        return DBHandler::getInstance()->delete($table, $where);
    }

    protected static function buildQuery($sql, $data = null)
    {
        return DBHandler::getInstance()->lowQuery($sql, $data);
    }

    protected static function fetch($resource)
    {
        return DBHandler::getInstance()->fetch($resource);
    }

    protected static function fetchAll($resource)
    {
        return DBHandler::getInstance()->fetch_all($resource);
    }

    public static function getLastInsertedID()
    {
        return DBHandler::getInstance()->getLastInsertedID();
    }
}

