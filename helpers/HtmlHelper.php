<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 08.09.2017
 * Time: 0:10
 */

namespace helpers;


use DateTime;

class HtmlHelper
{
    public static function loadJsFile($filename, $directory = null)
    {
        if (!is_null($directory))
            $directory = $directory . DS ;
        echo '<script src="'. DS . ConfigHelper::get('app', 'public_url') . DS . 'js' . DS . $directory . $filename . '.js" type="text/javascript"></script>';
    }

    public static function loadHtml($filename, $directory = null)
    {
        if (is_null($directory))
            $directory = 'site';
        return require_once DS . 'views'. DS . $directory. DS . $filename . '.php';
    }

    public static function disabled($flag)
    {
        return $flag ? 'disabled' : null;
    }

    public static function active($flag)
    {
        return $flag ? 'active' : null;
    }

    public static function pagination($urlParams = null, $currentOffset, $numberOfOffsets)
    {
        $currentOffset = (int) $currentOffset;
        $numberOfOffsets = (int) $numberOfOffsets;

        $url = (!is_null($urlParams)) ? http_build_query($urlParams) : null;

        $result = "<li class='". self::disabled($currentOffset == 1) ."'><a href='?". $url ."&offset=" . ($currentOffset - 1) . "'>&laquo;</a></li>";
        for ($i = 1; $i <= $numberOfOffsets + 1; $i++)
        {
            if ($i < 4 || $numberOfOffsets - $i < 3 || $i - abs($currentOffset) == 1 || $i == $currentOffset)
                $result .= '<li class="'. self::active($i == $currentOffset) .'"><a href="?'. $url .'&offset=' . $i . '">' . $i. '</a></li>';
        }
        $result .= '<li><a class="' . self::disabled($currentOffset == $numberOfOffsets) . '" href="?'. $url .'&offset=' . ($currentOffset + 1) . '">&raquo;</a></li>';

        return '<ul class="pagination pagination-sm">' . $result . '</ul>';
    }

    public static function hidden($flag)
    {
        return ($flag) ? ' hidden ' : null;
    }

    public static function filialAddress($street, $building)
    {
        return (!empty($street) && !empty($street)) ? $street .  ', д.' . $building : null;
    }

    public static function description($text)
    {
        return (mb_strlen($text) > 144) ? substr($text, 0, 144) . '...' : $text;
    }
}