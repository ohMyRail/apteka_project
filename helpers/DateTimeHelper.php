<?php

namespace helpers;


use DateInterval;
use DateTime;

class DateTimeHelper
{
    public static function setTimeOfDay(DateTime $dateTime, $hours, $minutes, $second)
    {
        $dateTime->setTime($hours, $minutes, $second);
        return $dateTime;
    }

    public static function setDateTime($dateTimeString)
    {
        return new DateTime($dateTimeString);
    }

    //example: 2017-08-30 18:45
    public static function getDateTimeFormat(DateTime $dateTime)
    {
        return $dateTime->format('y-m-d H:i');
    }

    public static function getHour(DateTime $dateTime)
    {
        return (int) $dateTime->format('H');
    }

    //example: 18:45
    public static function getTime(DateTime $dateTime)
    {
        return $dateTime->format('H:i');
    }

    //метод позволяет получить номер дня недели: 1 - понедельник, ..., 7 - воскресенье
    public static function getDayNumber(DateTime $dateTime)
    {
        return $dateTime->format('N');
    }

    public static function getDayOfWeek(DateTime $dateTime)
    {
        return strtolower(substr($dateTime->format('l'), 0, 2));
    }
    //example: 'P2Y4DT6H8M'
    public static function addInterval(DateTime $dateTime, $intervalQuery)
    {
        $newDateTime = new DateTime();
        $newDateTime->setTimestamp($dateTime->getTimestamp());

        $newDateTime->add(new DateInterval($intervalQuery));
        return $newDateTime;
    }

    public static function getInterval(DateTime $dateTime1, DateTime $dateTime2, $differenceType)
    {
        $difference = $dateTime1->diff($dateTime2);

        return $difference->$differenceType;
    }

    public static function getIntervalArray($date, $time, $isHourly)
    {
        if ($isHourly)
        {
            $dateTimeFrom =  self::setDateTime($date . ' ' . $time);
            $dateTimeTo = self::addInterval($dateTimeFrom, "PT1H");
        }
        else
        {
            $timeFrom = null;
            $timeTo = null;

            switch ($time) {
                case "morning": {
                    $timeFrom = '06:00';
                    $timeTo = '12:00';
                    break;
                }
                case "day": {
                    $timeFrom = '12:00';
                    $timeTo = '18:00';
                    break;
                }
                case "evening": {
                    $timeFrom = '18:00';
                    $timeTo = '24:00';
                    break;
                }
                case "night": {
                    $timeFrom = '24:00';
                    $timeTo = '06:00';
                    break;
                }
                default: {
                    return null;
                }
            }

            $dateTimeFrom = self::setDateTime($date . ' ' . $timeFrom);
            $dateTimeTo = self::setDateTime($date . ' ' . $timeTo);
        }

        return [
            'from' => $dateTimeFrom,
            'to' => $dateTimeTo
        ];
    }

    public static function toNormalDate($date, $show_time = true) {

        $date = strtotime($date);

        $days = array('', 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек');


        $newDate = date('d', $date) . ' ';
        $newDate .= $days[(int) date('m', $date)] . ' ';
        $newDate .= date('Y', $date) . ' ';
        if($show_time)
            $newDate .= date('H', $date) . ':' . date('i', $date);


        return $newDate;
    }

    //список сокращенных названий дней недели
    public static function getDaysOfWeekList()
    {
        return [
            1 => 'Пн',
            2 => 'Вт',
            3 => 'Ср',
            4 => 'Чт',
            5 => 'Пт',
            6 => 'Сб',
            7 => 'Вс'
        ];
    }

    //принимает время в формате 18:45:37, возвращает в формате 18:45
    public static function toTimeFormat($time)
    {
        return substr($time, 0, -3);
    }

    public static function getYear(DateTime $dateTime)
    {
        return $dateTime->format('Y');
    }
}