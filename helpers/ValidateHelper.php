<?php

namespace helpers;

class ValidateHelper
{
    const CREATE = true;
    const UPDATE = false;

    public static function validateSearchParams()
    {
        $limit = ConfigHelper::getCompleteListSize();
        $offset = (filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT) - 1);

        return [
            'keyword' => filter_input(INPUT_GET, 'keyword', FILTER_SANITIZE_STRING),
            'limit' => $limit,
            'offset' =>  (($offset >= 0) ? $offset : 0) * $limit,
        ];
    }

    public static function validateString($string)
    {
        return filter_var($string, FILTER_SANITIZE_STRING);
    }

    public static function validateLimitParams($offset)
    {
        $limit = ConfigHelper::getCompleteListSize();
        $offset = filter_var($offset, FILTER_SANITIZE_NUMBER_INT) - 1;

        return [
            'limit' => $limit,
            'offset' => (($offset >= 0) ? $offset : 0) * $limit,
        ];
    }
}