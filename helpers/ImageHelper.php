<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 30.08.2017
 * Time: 16:47
 */

namespace helpers;


class ImageHelper
{
    public static function getCompanyLogo($companyId)
    {
        if (file_exists(ROOT . DS . 'uploads' . DS . 'companies' . DS . $companyId . DS . 'logo.jpg'))
            return '/uploads/companies/' . $companyId . "/logo.jpg";

        return null;
    }

    public static function getProductLogo($productId)
    {
        if (file_exists(ROOT . DS . 'uploads' . DS . 'products' . DS . $productId . DS . 'logo.jpg'))
            return '/uploads/products/' . $productId . "/logo.jpg";

        return null;
    }

    public static function buildImageSource($data)
    {
        return (isset($data)) ? $data : null;
    }
}