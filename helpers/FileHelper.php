<?php

namespace helpers;

class FileHelper
{
    public static function isJsFileExist($filename)
    {
        return file_exists(ROOT . DS . ConfigHelper::get('app', 'public_url') . DS . 'js' . DS . $filename . '.js');
    }
}