<?php

namespace helpers;

class CountingHelper
{
    public static function getProximity($valueX1, $valueY1, $valueX2, $valueY2)
    {
        return abs(((double)$valueX1 - (double)$valueX2) * ((double)$valueX1 - (double)$valueX2) - ((double)$valueY1 - (double)$valueY2) * ((double)$valueY1 - (double)$valueY2));
    }

    public static function hasMoreData($offset, $currentDataCount, $allDataCount)
    {
        return ($offset + $currentDataCount < $allDataCount);
    }

    public static function getNumberOfOffsets($size, $allDataCount)
    {
        return (int) $allDataCount / $size;
    }
}