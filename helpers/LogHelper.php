<?php

namespace helpers;

class LogHelper
{
    public static function log($data) {
        if (ConfigHelper::isDebugMode()) {
            $data = date('[Y-m-d H:i:s] - ') . print_r($data, 1) . PHP_EOL;
            file_put_contents('log/log.txt', $data, FILE_APPEND);
        }
    }

}