<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 29.08.2017
 * Time: 15:49
 */

namespace helpers;


class ConfigHelper
{
    private static $_config = array();

    public static function set()
    {
        if (empty(self::$_config))
        {
            if (file_exists(ROOT . DS . 'config/main.php'))
                self::$_config = require_once ROOT . DS . 'config/main.php';
            else
                exit("The main config file not found");
        }
    }

    /**
     * @param string $group название группы настроек
     * @param string $item название пункта настройки
     * @return string
     */
    public static function get($group, $item)
    {
        return self::$_config[$group][$item];
    }

    /**
     * @param string $group название группы настроек
     * @return array
     */
    public static function getGroup($group = null)
    {
        return ($group == null) ? self::$_config : self::$_config[$group];
    }

    public static function getPrefix()
    {
        return self::$_config['db']['pref'];
    }

    public static function getCompleteListSize()
    {
        return self::$_config['app']['complete_list_size'];
    }

    public static function getIncompleteListSize()
    {
        return self::$_config['app']['incomplete_list_size'];
    }

    public static function getAccountListSize()
    {
        return self::$_config['app']['account_list_size'];
    }

    public static function publicUrl()
    {
        echo self::$_config['app']['base_url'] . '/' . self::$_config['app']['public_url'] . '/';
    }

    public static function getSecureHashKey()
    {
        return self::$_config['secure']['hashKey'];
    }

    public static function isDebugMode()
    {
        return self::$_config['core']['mode'] == 1;
    }
}