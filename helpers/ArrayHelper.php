<?php
/**
 * Created by PhpStorm.
 * User: Раиль
 * Date: 29.08.2017
 * Time: 19:50
 */

namespace helpers;


class ArrayHelper
{
    public static function listToString(array $list)
    {
        return implode(', ', $list);
    }

    public static function arrayToList(array $data, $fieldName)
    {
        $list = [];

        foreach ($data as $key => $value)
            $list[$key] = $value[$fieldName];

        return $list;
    }

    public static function rebuildArray(array $data, $primaryFieldName)
    {
        $result = [];

        foreach ($data as $key => $value){
            $resultData = [];

            foreach ($value as $field => $item)
                if ($field != $primaryFieldName)
                    $resultData[$field] = $item;

            $result[$value[$primaryFieldName]] = $resultData;
            unset($resultData);
        }
        return $result;
    }

    public static function merge(array $first, array $second)
    {
        return array_merge($first, $second);
    }

    //Проверяет, заполнен ли массив ключами, переданными в параметрах
    public static function isFilledInArray(array $array, array $keys)
    {
        foreach ($keys as $key) {
            if (!(array_key_exists($key, $array) and !empty($array[$key]))) {
                return false;
            }
        }

        return true;
    }


}